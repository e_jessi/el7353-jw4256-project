#!/bin/bash
ip=$1
rto_min=$2
echo $(cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys)
printf "%s\n" "cat public key"
sleep 5s
echo $(tar -xzf iproute2-4.5.0.tar.gz)
printf "%s\n" "tar iproute2"
sleep 5s
echo $(sudo apt-get update)
printf "%s\n" "print y"
sleep 2s
echo $(sudo apt-get build-dep iproute2)
sleep 5s
printf "%s\n" "built"
echo $(cd iproute2-4.5.0/ && ./configure)
sleep 5s
printf "%s\n" "configure"
echo $(cd iproute2-4.5.0/ && make)
sleep 6s
printf "%s\n" "make"
echo $(cd iproute2-4.5.0/ && sudo make install)
printf "%s\n" "install"
sleep 3s
echo $(cd iproute2-4.5.0/ && sudo ldconfig)
sleep 3s
echo $(sudo ip route change 10.0.0.0/8 via 10.10.$ip.1 dev eth1 rto_min $rto_minms)
printf "%s\n" "changeroute"
echo $(cd app && make)
printf "%s\n" "make"
sleep 5s
echo $(sudo apt-get update)
echo $(sudo apt-get install iperf)
sleep 5s
printf "%s\n" "Done"
echo $(ip route)
