##Introduction
This experiment is about reproducing TCP Incast Throughput Collapse on GENI testbed. 

For an overview of the project, please refer to this [paper](http://dl.acm.org/citation.cfm?id=1592693) :
Chen, Yanpei, et al. "Understanding TCP incast throughput collapse in datacenter networks." Proceedings of the 1st ACM workshop on Research on enterprise networking. ACM, 2009.

It will take approximately 3.5 hours to run this experiment and reproduce the results.

Datacenters support a large number of service and applications. Many companies like Google, Yahoo, Microsoft and Amazon use datacenter for web search, e-commerce and large-scale general computations. Most datacenters use TCP to communicate between nodes. As we all know, TCP is a mature technology that fits most communications between applications. However, the original TCP cannot meet the requirement of unique workloads, scale and environment of datacenter. We find some new disadvantages about TCP in high-bandwidth, low-latency datacenter environment. This is the interesting reason that we reproduced the Incast problem.



##Background

Congestion in networks can cause serious TCP throughput collapse problem, especially in the high-bandwidth, low-latency datacenter environment. This phenomenon, which is also called TCP Incast, often occurs when the client synchronized receives data from multiple senders that together send enough data to client. Unfortunately there is still no efficient solution to overcome this shortcoming. Therefore I reproduced TCP Incast problem on testbed so that we could analyze the Incast problem thoroughly and try to present an exhaustive discussion of Incast.

I reproduced the result in Figure 5 from this [paper](http://dl.acm.org/citation.cfm?id=1592693):

![](http://i4.piimg.com/700eedad492a9ecc.png)

This figure was obtained by running experiment with five different minimum RTO timer values. As we can see, the figure is divided into three regions for up to 48 senders. Region 1(R1) shows an initial goodput collapse as the number of senders increases. Region 2(R2) shows a goodput recovery. Region 3(R3) illustrates as the number of senders increases, goodput decreases again after goodput recovery.

There are many details in this goodput figure. 

For the first region, we should notice that smaller minimum RTO timer values indicate larger initial goodput minimum. What’s more, the initial goodput minimum happens at the same number of concurrent senders.

Region 2 indicates that smaller minimum RTO timer values could recover faster.

From the last region we can see that for all minimum RTO timer values, after the goodput maximum, the slope of goodput decrease is the same.We can find a common characteristic of throughput and understand the Incast problem better during reproducing experiment.

##Result

In this section, I present my results of reproducing Incast and their  comparisons with original figure (The origin is given in the Backgroun section).

Taking into consideration what we have said about Incast problem, it depends very much on having a large number of concurrent senders as well as having high-capacity links. However, I cannot get a topology with a large number of nodes and high-capacity links at the same time. So I run my experiments in both two situations separately.


Figure 3. presents the goodput for five different values of minimum RTO with up to 30 senders. Bandwidth of each link is 100Mbps.



![](http://7xrn7f.com1.z0.glb.clouddn.com/16-5-2/56595983.jpg)
######Figure 3. Goodput for various minimum RTO values ( 30 senders, 100Mbps Bandwidth)

At the beginning, the throughput with these three different values of min RTO all occurred an initial collapse. Then a goodput recovery was followed by an unobvious goodput decrease. Besides, a smaller minimum RTO timer value shows larger goodput values for the initial minimum. And different minimum RTO timer values showed different location of turning points and different slope. 

The results of 40ms min RTO, 100ms min RTO and 200ms min RTO are nearly identical to the ideal figure. For the remaining two minimum RTO timer values (1ms and 10ms), the result didn’t show an ideal result. This may because the bandwidth of links is not high enough. As we aforementioned, TCP Incast problem often occurs in the high-bandwidth environment.

Figure 4. presents the goodput for five different values of minimum RTO with up to 12 senders. Bandwidth of each link is 1Gbps.

![](http://7xrn7f.com1.z0.glb.clouddn.com/16-5-2/8982802.jpg)
######Figure 4. Goodput for various minimum RTO values ( 12 senders, 1Gbps Bandwidth)

This experiment runs in high bandwidth. Thus it is very similar to two front regions of originals. However, there are only 12 senders, we can only see the initial throughput collapse and the goodput recovery. The second goodput decrease shown in ideal figure cannot be seen since it occurs at least there are 16 senders.


##Run my experiment

####Step 1: Set up a topology on GENI

Log on GENI, create a new slice. In Jack, add resources by uploading the experiment RSpec document which is given in my repository. Then choose an aggregate and reserve resources. Wait until all nodes turn green and be available to login.

####Step 2: Download the code

Download the experiment code which is named app to your own computer. Here we need to edit app/scripts/node_list_all.txt to make sure all IP addresses in it are your servers' IP addresses.

####Step 3: Transfer the code to every nodes

Open a terminal on your own computer, run the command 

    ./transfer_file PORT PC

where PORT is the port number of the node, PC is the number of pc in node's hostname.

For example, in this formate "jw4256@pc4.instageni.idre.ucla.edu -p 32058", the PORT is 32058 and  the PC is 4.

We should notice in this transfer_file script, the path of code should be motified according to the location where we download our code.

####Step 4: Generate SSH key on client node

Login to the client node, run the command
     
     ssh-keygen -t rsa
  
Just press < Enter> to accept the default location and file name.

Enter, and re-enter, a passphrase when prompted.

####Step 5: Load public key to your own computer

Open a terminal on your own computer, run the command:

      scp -P port username@hostname: ~/.ssh/id_rsa.pub ~/key30

####Step 6: Transfer public key to all servers

Run the command:

    ./transfer_key PORT PC

where PORT is the port number of the node, PC is the number of pc in node's hostname.

For example, in this formate "jw4256@pc4.instageni.idre.ucla.edu -p 32058", the PORT is 32058 and  the PC is 4.

####Step 7: Transfer command scripts to all nodes

Run the command:

    ./transfer_command PORT PC

where PORT is the port number of the node, PC is the number of pc in node's hostname.

This command script includes all commands that need to be run on nodes.
####Step 8: Downlode iproute2 to your own computer

Download the most recent version of iproute2 (2.4.5.0 as of right now) in a tar.gz archive from [iproute2](https://www.kernel.org/pub/linux/utils/net/iproute2/) onto your node.


####Step 9: Transfer iproute2 to all nodes

Run the command:

    ./transfer_iproute2 PORT PC

where PORT is the port number of the node, PC is the number of pc in node's hostname.

####Step 10: Check bandwidth of each link

Log on one server node, and run
 
        iperf -s
       
Then run this command on client node
         
         iperf - t 60 -i 10 -c server_IP_address
         
After it ends, check and record bandwidth of each link

####Step 11: Run command script on all nodes

Log on all nodes, run the command:
  
      ./command IP RTO_MIN
      
where IP is the ip address of that node, rto_min is 100 

####Step 12: Change value of rto_min on router

Transfer change_rto script to router node, then run this command on router:
 
         ./change_rto RTO_MIN
         
where RTO_MIN is the value of rto_min that you want to change.

####Step 13: Run code on client

Log on client node, run:

        cd app/scripts
        sh _run_all_wrapper2.sh

####Step 14: Record the results

Record goodput of each sender while client is running code.

####Step 15: Change the value of rto_min

Open a terminal on your own computer, and run:
 
         ./transfer_command2 PORT PC
         
where PORT is the port number of the node, PC is the number of pc in node's hostname.

Then log on client and all servers nodes, and run:
 
         ./command2 IP MIN
         
where IP is the ip address of that node, MIN is the value of rto_min that you want to change.

####Step 16: Repeat doing experiment with different value of rto_min

Repeat step 12--15

####Step 17: Experiment2 with 12-server and 1Gbps bandwidth

These two experiments are very similar, just change the host name of each node in all scripts and repeat step 1--16.

##Notes
* For the experiment1 with 30-server, I used UCLA InstaGENI aggregate.

* For the experiment2 with 12-server, I used Apt aggregate.

* For more information of code, please refer to [here](https://github.com/amarp/incast)

* Notice for all scripts and code, you should check the path of some files and IP address in it.

* GENI testbed doesn't supply large numbers of nodes and high bandwidth at the same time. 

     * For high bandwidth, I set bandwidth 1Gbps. In this situation, ExoGENI is more reliable than InstallGENI.
      
     * For large numbers of nodes, Apt is more reliable. I used to set up a 48-server topology, but it is very instable. The maximum of nodes I set is 30.
     * You may check [here](https://flsmonitor.fed4fire.eu/geni_tutorial.html) to see current aggregate status.
     * If you are going to use Apt aggregate, then you don't need to install iproute2. Because the command "sudo ip route change xx.xx.xx.xx rto_min xms" can be run well with Apt aggregate. Then you should do a little modification in command scripts by deleting line7--line26, which are contents about iproute2.
     * Notice Ip address of s2 in experiment1 is different, so change rto_min on that node seperately.